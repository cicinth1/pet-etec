const mongoose = require ('../database');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true,
    },
    email: {
        type: String,
        required: true, 
    }, 
    password: {
        type: String,
        required: true, 
    },  
    status : {
        type: String,
        required: true, 
    },
    createAt: {
        type: Date, 
        default: Date.now,  
        required: true,
    },
  
});


const User = mongoose.model('User', userSchema);



module.exports = User;
