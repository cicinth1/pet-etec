const express = require ('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const User = require ('../../models/Users');

router.post('/login', async(request, response)=>{
    const email = request.body.email
    const password = request.body.password

    const user = await User.findOne({"email":email})
    const hash = user.password

    if(bcrypt.compareSync(password, hash)) {
        response.send("Autenticação ok")
       } else {
        response.send("falha na autenticação")
       }
})

module.exports = app => app.use(router)