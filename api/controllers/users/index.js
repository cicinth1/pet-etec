const express = require ('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const User = require ('../../models/Users');

router.post('/user-register', async(request, response)=>{

    const NewUser =  request.body
    
    NewUser.status = "ativo"

    NewUser.password = bcrypt.hashSync(request.body.password, 10);

    const user =  await User.create(NewUser)
    
    response.send(user) 
})
router.get('/users', async(request, response)=>{
    
    const users = await User.find()  
    
    response.send(users)
   
})
module.exports = app => app.use(router)